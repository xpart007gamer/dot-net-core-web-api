﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Spidrs.DataAccess.Repository.IRepository;
using Spidrs.DataAccess;
using Spidrs.Models.ViewModels;
using System.Data;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Account.Internal;

namespace Spidrs.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ContainerController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IUserStore<IdentityUser> _userStore;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<RegisterModel> _logger;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;


        public ContainerController(
           UserManager<IdentityUser> userManager,
           IUserStore<IdentityUser> userStore,
           SignInManager<IdentityUser> signInManager,
           ILogger<RegisterModel> logger,
         IUnitOfWork unitOfWork,
         ApplicationDbContext context,
           RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _userStore = userStore;
            _unitOfWork = unitOfWork;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _roleManager = roleManager;

        }
        public IActionResult Index()
        {

            return View();
        }
        [Produces("application/json")]
        [HttpPost]
        public ActionResult GetContainers()
        {
            List<ContainerList> data = new List<ContainerList>();
            //var start = 1;
            //var Length = 10;
            //var searchvalue = "";
            //var sortcoloumnIndex = 0;
            //var SortColumn = "";
            //var SortOrder = "";
            //var recordsTotal = 0;

            var start = (Convert.ToInt32(Request.Form["start"]));
            var Length = (Convert.ToInt32(Request.Form["length"])) == 0 ? 10 : (Convert.ToInt32(Request.Form["length"]));
            var searchvalue = Request.Form["search[value]"];
            var sortcoloumnIndex = Convert.ToInt32(Request.Form["order[0][column]"]);
            var SortColumn = "";
            var SortOrder = "";
            var sortDirection = Request.Form["order[0][dir]"].ToString();
            var recordsTotal = 0;
            switch (sortcoloumnIndex)
            {
                case 0:
                    SortColumn = "containerID";
                    break;
                case 1:
                    SortColumn = "container";
                    break;
                case 2:
                    SortColumn = "accountname";
                    break;
                case 3:
                    SortColumn = "dayofweek";
                    break;
                case 4:
                    SortColumn = "maintained";
                    break;
            }

            data = _unitOfWork.Container.ContainerList(start, searchvalue, Length, SortColumn, SortOrder).ToList();

            recordsTotal = data.Count > 0 ? data[0].TotalRecords : 0;

            return Json(new { data = data, recordsTotal = recordsTotal, recordsFiltered = recordsTotal });
        }
    }
}
