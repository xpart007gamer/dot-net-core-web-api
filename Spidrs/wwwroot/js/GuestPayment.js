﻿$(document).ready(function () {

    var tblcustomer = $('#tblGuestPayment').DataTable({

        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: 'Next &raquo;',
                previous: '&laquo; Previous'
            }
        },
        "pagingType": "input",
        "searching": true,
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": [],
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }]
    });
    $(".loader-wrapper").removeClass("show1");
    SearchText();
});

function SearchText() {



    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary

    
    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}
function AddForSkipcheckbox(checkbox, CustomerServiceID) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#hdSkipcheckedList").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + CustomerServiceID;
        } else
            hdcheckedList = CustomerServiceID;

        $("#hdSkipcheckedList").val(hdcheckedList);
    }
    else {
        var single = $("#hdSkipcheckedList").val()
        var delList = $("#hdSkipcheckedList").val().split(',');
        var index = delList.indexOf(CustomerServiceID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdSkipcheckedList").val(bbb);
    }
    SkipCheckbox(CustomerServiceID);
}
function AddForSkipuncheckbox(checkbox, CustomerServiceID) {

    if ($(checkbox).is(':checked')) {
        var single = $("#hdSkipUncheckList").val()
        var delList = $("#hdSkipUncheckList").val().split(',');
        var index = delList.indexOf(CustomerServiceID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdSkipUncheckList").val(bbb);
    }
    else {

        var hdUncheckList = $("#hdSkipUncheckList").val();

        if (hdUncheckList != "") {
            hdUncheckList = hdUncheckList + ',' + CustomerServiceID;
        } else
            hdUncheckList = CustomerServiceID;

        $("#hdSkipUncheckList").val(hdUncheckList);
    }
    SkipCheckbox(CustomerServiceID);
}
function AddForApplycheckbox(checkbox, CustomerServiceID) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#hdApplycheckedList").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + CustomerServiceID;
        } else
            hdcheckedList = CustomerServiceID;

        $("#hdApplycheckedList").val(hdcheckedList);
    }
    else {
        var single = $("#hdApplycheckedList").val()
        var delList = $("#hdApplycheckedList").val().split(',');
        var index = delList.indexOf(CustomerServiceID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdApplycheckedList").val(bbb);
    }
}
function SkipCheckbox(id) {

    $("input[value='" + id + "'][type='checkbox']").change(function () {
        $("input[value='" + id + "'][type='checkbox']").prop("checked", $(this).prop("checked"));
    });

}
function UpdateGuestPayment() {
    var Skipid = '';
    var unSkipdid = '';
    var Applyid = '';
    unSkipdid = $("#hdSkipUncheckList").val();
    Skipid = $("#hdSkipcheckedList").val();
    Applyid = $("#hdApplycheckedList").val();
    //var table = $('#tblGuestPayment');
    //table.DataTable().destroy();

    //$("#tblGuestPayment TBODY TR").each(function (index, value) {
    //    var row = $(this);

    //    var checkSkipstatus = row.closest('tr').find('.chkRowSkip').prop('checked');
    //    if (checkSkipstatus == true) {
    //        Skipid += (!Skipid ? row.find(".chkRowSkip").val() : (',' + row.find(".chkRowSkip").val()));

    //    }
    //    else {

    //        unSkipdid += (!unSkipdid ? row.find(".chkRowSkip").val() : (',' + row.find(".chkRowSkip").val()));
    //    }

    //    var checkApplystatus = row.closest('tr').find('.chkRowApply').prop('checked');
    //    if (checkApplystatus == true) {
    //        Applyid += (!Applyid ? row.find(".chkRowApply").val() : (',' + row.find(".chkRowApply").val()));

    //    }

    //});
    $(".loader-wrapper").addClass("show1");
    var hdUpdateGuestPayment = $('#hdUpdateGuestPayment').val();

    $.ajax({
        type: "Post",
        url: hdUpdateGuestPayment,
        data: { Skipids: Skipid, unSkipdids: unSkipdid, Applyids: Applyid },
        success: function (result) {
            $("div.updatemsg").show();
            $(".loader-wrapper").removeClass("show1");
            setTimeout(function () {

                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);


        },
        error: function (response) {

        }
    });

}

$("#chkSkipAll").click(function () {

    //Determine the reference CheckBox in Header row.
    var chkAll = this;

    //Fetch all row CheckBoxes in the Table.
    var chkRows = $("#tblGuestPayment").find(".chkRowSkip");

    //Execute loop over the CheckBoxes and check and uncheck based on
    //checked status of Header row CheckBox.
    chkRows.each(function () {
        $(this)[0].checked = chkAll.checked;

    });
    if (chkAll.checked == true) {
        var values = "";

        $("#tblGuestPayment TBODY TR").each(function (index, value) {
            var row = $(this);

            var checkstatus = row.closest('tr').find('.chkRowSkip').prop('checked');

            values += (!values ? row.find(".chkRowSkip").val() : (',' + row.find(".chkRowSkip").val()));
            $("#hdSkipcheckedList").val(values);
        });

    }
    else {
        $("#hdSkipcheckedList").val(values);
    }
});

$("#chkApplyAll").click(function () {



    //Determine the reference CheckBox in Header row.
    var chkAll = this;

    //Fetch all row CheckBoxes in the Table.
    var chkRows = $("#tblGuestPayment").find(".chkRowApply");

    //Execute loop over the CheckBoxes and check and uncheck based on
    //checked status of Header row CheckBox.
    chkRows.each(function () {
        $(this)[0].checked = chkAll.checked;

    });
    if (chkAll.checked == true) {
        var values = "";

        $("#tblGuestPayment TBODY TR").each(function (index, value) {
            var row = $(this);

            var checkstatus = row.closest('tr').find('.chkRowApply').prop('checked');

            values += (!values ? row.find(".chkRowApply").val() : (',' + row.find(".chkRowApply").val()));

            $("#hdApplycheckedList").val(values);
            $("#hdSelectAllApplyStatus").val(1);

        });

    }
    else {

        $("#hdApplycheckedList").val(values);
        $("#hdSelectAllApplyStatus").val(0);
    }

});
function SaveAll() {
    
    var values = $("#hdSelectAllApplyStatus").val();
    if (values == 1) {
        $("#myModalpop").show();
    }
    else {
        UpdateGuestPayment();

    }
}

function hidepopuncheck() {
    $("#myModalpop").hide();
}



function ShowModalDecline(BankPaymentID) {

    $('#txtReasonCode').text();
    var url = $('#hdGetReasonText').val();
    
    $.ajax({
        type: "Post",
        url: url,
        data: { BankPaymentID: BankPaymentID },
        success: function (result) {
            
            $('#hfBankPaymentID').val(BankPaymentID);

            if (result[0].paymentRejectReason != null) {
                $('#txtReasonText').text(result[0].paymentRejectReason);
            }
            else {
                $('#txtReasonText').text();
            }
            if (result[0].reasonCode != null) {
                $('#txtReasonCode').text(result[0].reasonCode);
            }
            else {
                $('#txtReasonCode').text('');
            }
            $("#MyModalDecline").show();
        },
        error: function (response) {
        }
    });


}

function SaveReason() {

    var BankPaymentID = $('#hfBankPaymentID').val();
    var ReasonText = $('#txtReasonText').val();
    var ReasonCode = $('#txtReasonCode').val();
    var url = $('#hdSaveReason').val();
    
    $.ajax({
        type: "Post",
        url: url,
        data: { BankPaymentID: BankPaymentID, ReasonText: ReasonText, ReasonCode: ReasonCode },
        success: function (result) {
            
            if (result == true) {
                $("div.success").show();
                $("div.success").fadeIn(300).delay(1500).fadeOut(4000);
                $("#MyModalDecline").hide();

                setTimeout(function () {
                    var url = $("#hdPaymentDeclined").val();
                    window.location.href = url;
                }, 3000);
            }
        },
        error: function (response) {
        }
    });


}

function CloseModalDecline() {
    $("#MyModalDecline").hide();
}
