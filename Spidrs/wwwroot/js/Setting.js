﻿//function SaveallPermissionPage() {
//   
//    var RoleName = $('#RoleName').val();
//    if (RoleName == "") {
//        $('#RoleName').css('border-color', 'red');
//        $('#RoleName').focus();
//        return false;
//    }
//    var customer = [];
//    var formData = new FormData();
//    formData.append("RoleName", RoleName);
//    var j = 0;
//    $("#permissiontable TBODY TR").each(function (index, value) {
//        var row = $(this);
//        customer.PermissionId = row.find(".PermissionIdc").text();
//        customer.IPageName = row.find(".PageNamec").text();
//        customer.IPermissioncheck = $(this).closest('tr').find('.Permissioncheckc').prop('checked');
        
//        formData.append("multidata[" + j + "].PermissionId", customer.PermissionId);
//        formData.append("multidata[" + j + "].PageName", customer.IPageName);
//        formData.append("multidata[" + j + "].IsPermission", customer.IPermissioncheck);
//        j++;
//    });
    
//    $.ajax({
//        type: "Post",
//        url: "/Settings/AddRole",
//        contentType: false,
//        processData: false,
//        data: formData,
//        success: function (result) {
           
//            window.location.href = "https://localhost:44351/Settings/RolesPermission";
//        },
//        error: function (response) {

//        }
//    });
//}

//function UpdatePermissionPage() {
//   
//    var Roloid = $('#Roloid').val();
//    var customer = [];
//    var formData = new FormData();
//    formData.append("Rolid", Roloid);
//    var j = 0;
//    $("#permissiontable TBODY TR").each(function (index, value) {
//        var row = $(this);
//        customer.PermissionId = row.find(".PermissionIdc").text();
//        customer.IPageName = row.find(".PageNamec").text();
//        customer.IPermissioncheck = $(this).closest('tr').find('.Permissioncheckc').prop('checked');

//        formData.append("multidata[" + j + "].PermissionId", customer.PermissionId);
//        formData.append("multidata[" + j + "].PageName", customer.IPageName);
//        formData.append("multidata[" + j + "].IsPermission", customer.IPermissioncheck);
//        j++;
//    });

//    $.ajax({
//        type: "Post",
//        url: "/Settings/EditRolesPermission",
//        contentType: false,
//        processData: false,
//        data: formData,
//        success: function (result) {
//            window.location.href = "https://localhost:44351/Settings/RolesPermission";
//        },
//        error: function (response) {

//        }
//    });
//}


///Email Check
$(document).ready(function () {
    SearchText();
});
function SearchText() {



    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary

    
    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}
function Emailcheck() {
    var Email = $('#backofficeemail').val();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(Email)) {
        $("#erroremail").html("Please Enter valid email").show().fadeOut(3000);
        return false;
    }
    var url = $('#hdsnCheckexistemail').val();
    $.ajax({
        type: "GET",
        url: url,
        data: { initialEmailID: Email },
        success: emailanswer,
        error: function (xhr, status, error) {
            alert(error);
        }
    });
    function emailanswer(ResultData) {
       
        if (ResultData == false) {
            $("#erroremail").html("This Email ID is already exist").show();
            $("#backofficeemail").val("");
        }
        else {

            $("#erroremail").hide();
        }
    }
}

function SendMailBackOfficeUser() {
   
    var backofficeemail = $('#backofficeemail').val();
    if (backofficeemail == "") {
        $('#backofficeemail').css('border-color', 'red');
        $('#backofficeemail').focus();
        return false;
    }
    $(".pdfs").css("opacity", 0.5);
    $(".pdfs").css("background-color", "A9A9A9");
    var formData = new FormData();
    formData.append("backofficeemail", backofficeemail);
    var url=$('#hdsnSendMailBackOfficeUsers').val();
    $.ajax({
        type: "Post",
        url: url,
        data: { Email: backofficeemail},
        success: function (result) {
            if (result == true) {

                $(".pdfs").css("opacity", "");
                $(".pdfs").css("background-color", "");
                $("#inviteuser").modal("hide")
                $("div.success").show();
                $("div.success").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdUserList").val();
                    window.location.href = url;
                }, 3000);
            }
            else {
                $(".pdfs").css("opacity", "");
                $(".pdfs").css("background-color", "");
                $("#inviteuser").modal("hide")
                $("div.failure").show();
                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdUserList").val();
                    window.location.href = url;
                }, 3000);
            }
        },
        error: function (response) {
        }
    });
}

function confirm1(elem) {
   
    var id = $(elem).data('assigned-id');
    $('#cuids').val(id);
    $('#myModal45656').modal('show');
}
var Delete = function () {
   
    var id = $("#cuids").val();
    var deleteurl = $('#hdsnDeleteRolesPermission').val();
    $.ajax({
        type: "Post",
        url: deleteurl,
        data: { Rolid: id },
        success: function (result) {
            $('#myModal45656').modal('hide');
            $("div.amountfailure").show();
            $("div.amountfailure").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {
                var url = $("#hdRolesPermission").val();
                window.location.href = url;
            }, 3000);
        }
    })
}


function confirmdelete(elem) {
   
    var id = $(elem).data('assigned-id');
    $('#backofficeid').val(id);
    $('#myModaluserlist').modal('show');
}
var Deletebackoffice = function () {
   
    var id = $("#backofficeid").val();
    var deleteurl = $('#hdsnDeletebackoffice').val();
    $.ajax({
        type: "Post",
        url: deleteurl,
        data: { backofficeid: id },
        success: function (result) {
            $('#myModaluserlist').modal('hide');
            $("div.amountfailure").show();
            $("div.amountfailure").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {
                var url = $("#hdUserList").val();
                window.location.href = url;
            }, 3000);
        }
    })
}