﻿ 
$(document).ready(function () {
    var statementTable = $('#example').DataTable({

        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [0, 8] },
            { "bSearchable": false, "aTargets": [0, 8] }
        ],
        "columns": [
            { "target": 1, "visible": false },
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "statementID" },
            { "data": "ServiceType" },
            { "data": "accountNumber" },
            { "data": "statementDate" },
            { "data": "dueDate" },
            { "data": "amountBilled" },
            { "data": "Action" }],

        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: '<i class="fa fa-angle-right"></i>',
                previous: '<i class="fa fa-angle-left"></i>'
            }
        },
        "searching": true,
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": []
    });
    $(".loader-wrapper").removeClass("show1");
    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
       
        var tr = $(this).closest('tr');
        var row = statementTable.row(tr);
        var url = $("#hdnStatementDetailListUrl").val();
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            var table = '';
            var statementId = row.data().statementID;
            $.ajax({
                type: "GET",
                dataType: "Json",
                cache: false,
                async: true,
                url: url,
                data: { statementID: statementId },
                contentType: "application/json; charset=utf-8",
                success: function (ds) {
                    table = '<table class="table dt-responsive" style="width:100%;text-align:center">' +
                        '<tr style="background-color:lightgray;">' +
                        '<th>TransactionType</th>' +
                        '<th>TransactionDate</th>' +
                        '<th>Description</th>' +
                        '<th>Amount</th>' +
                        '<th>Balance</th>' +
                        '</tr>'
                    var trow = '';
                    $.each(ds, function (a, b) {
                        trow += '<tr>' +
                            '<td>' + b.transactionType + '</td>' +
                            '<td>' + formatDate(b.transactionDate) + '</td>' +
                            '<td>' + b.description + '</td>' +
                            '<td>' + b.amount + '</td>' +
                            '<td>' + b.balance + '</td>' +
                            '</tr>'

                    });
                    '</table>';
                    table = table + trow;
                    if (table !== '') {
                        row.child(table).show();
                        tr.addClass('shown');
                    }

                },
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
        }
    });

    //$('#example tbody').on('click', '.btn', function () {
    //    var data = statementTable.row($(this).closest('tr')).data();
    //    $("#hdnAmountBilled").val(data.amountBilled);
    //    $("#hdnCustomerServiceId").val(data[0]);
    //   
    //    var url = $("#hdnWalletListUrl").val();
    //    $.ajax({
    //        type: "GET",
    //        url: url,
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "Json",
    //        success: function (response) {
    //            $('#WalletList').dataTable({
    //                data: response,
    //                "columns": [
    //                    {
    //                        "render": function (data, type, full, meta) {
    //                            return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
    //                        }
    //                    },

    //                    { "data": "bankName" },
    //                    { "data": "bankAccountNumber" },
    //                    { "data": "bankRoutingNumber" },
    //                    {
    //                        "render": function (data, type, full, meta) {
    //                            return ' <a href="#" id="walletDelete"><span class="fa fa-trash"></span></a>';
    //                        }
    //                    }
    //                ],
    //                "aoColumnDefs": [
    //                    { "bSortable": false, "aTargets": [4] },
    //                    { "bSearchable": false, "aTargets": [4] }
    //                ],

    //                language: {
    //                    "search": '<i class="fa fa-search"></i>',
    //                    searchPlaceholder: "Search records",
    //                    paginate: {
    //                        next: '<i class="fa fa-angle-right"></i>',
    //                        previous: '<i class="fa fa-angle-left"></i>'
    //                    }
    //                },

    //                "searching": true,
    //                "lengthChange": false,
    //                "paging": true,
    //                "info": true,
    //                "iDisplayLength": 3,
    //                "bDestroy": true

    //            });

    //            $('#wallet_list').modal('show');
    //        },
    //        failure: function (response) {
    //            alert(response.responseText);
    //        },
    //        error: function (response) {
    //            alert(response.responseText);
    //        }
    //    });
    //});
});

$('#btnPayment').on('click', function () {
   
    var customerServiceId = $("#hdnCustomerServiceId").val();
    var amount = $("#hdnAmountBilled").val();

    if (customerServiceId == "" || customerServiceId == undefined) {
        $("div.selectfailures").show();
        $("div.selectfailures").fadeIn(300).delay(1500).fadeOut(3000);
        return;
    }
    if (amount > 7500) {

        $("div.amountmax").show();
        $("div.amountmax").fadeIn(300).delay(1500).fadeOut(3000);
        return;
    }
    var autoPayId = $("#hdnAutoPayId").val();
    if (amount == 0 || amount == undefined) {
        $("div.amountfailure").show();
        $("div.amountfailure").fadeIn(300).delay(1500).fadeOut(3000);
        return;
    }
    if (autoPayId > 0 || autoPayId == undefined) {
        $("div.autoPayfailure").show();
        $("div.autoPayfailure").fadeIn(300).delay(1500).fadeOut(3000);
        return;
    }

   var ServiceAccountNumber = $("#hdnServiceAccountNumber").val();
    var url = $("#hdnWalletListUrl").val();
    $.ajax({
        type: "GET",
        url: url,
        data: { ServiceAccountNumber: ServiceAccountNumber},
        contentType: "application/json; charset=utf-8",
        dataType: "Json",
        success: function (response) {
            $('#WalletList').dataTable({
                data: response.walletInfos,
                "columns": [
                    {
                        "render": function (data, type, full, meta) {
                            $("#ach").prop("checked", true);
                            $('#achdiv').css('display', 'block')
                            $('#WalletListdiv').css('display', 'block')
                            $('#creditdiv').css('display', 'none')
                            $('#WalletListCreditdiv').css('display', 'none')
                            return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                        }
                    },

                    { "data": "bankName" },
                    { "data": "bankAccountNumber" },
                   
                    {
                        "render": function (data, type, full, meta) {
                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                        }
                    }
                ],
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [3] },
                    { "bSearchable": false, "aTargets": [3] }
                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },

                "searching": true,
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 3,
                "bDestroy": true

            });


            $('#WalletListCredit').dataTable({
                data: response.walletInfosCredit,

                "columns": [
                    {
                        "render": function (data, type, full, meta) {
                            if ($("#hwaletid").val() == full.walletInfoID) {
                                $("#credit").prop("checked", true);
                                $('#creditdiv').css('display', 'block')
                                $('#achdiv').css('display', 'none')
                                $('#WalletListdiv').css('display', 'none')
                                $('#WalletListCreditdiv').css('display', 'block')
                                return '<input type="radio" checked="checked"  id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                            }
                            else {
                                return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                            }

                        }
                    },
                    { "data": "creditCardHolderName" },
                    { "data": "creditCardNumber" },
                    { "data": "month" },
                    { "data": "year" },
                    {
                        "render": function (data, type, full, meta) {
                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                        }
                    }


                ],
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },

                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 3,
                "bDestroy": true

            });
            $('#wallet_list').modal('show');


        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });

});

$('#btnSave').on('click', function () {
    var WebPaymentsToken = $("#hdnWebPaymentsToken").val();
    var url = $("#hdnCreateWalletListUrl").val();
    var walletInfoes = new Object();
    walletInfoes.BankName = $("#WalletInfo_BankName").val();
    walletInfoes.BankAccountNumber = $("#WalletInfo_BankAccountNumber").val();
    walletInfoes.BankType = $("#ddlBankType").val();
    walletInfoes.BankHolderType = $("#ddlBankHolderType").val();
    walletInfoes.BankRoutingNumber = $("#WalletInfo_BankRoutingNumber").val();
    walletInfoes.ServiceAccountNumber = $("#hdnServiceAccountNumber").val();
    if (walletInfoes.BankName == "") {

        $("div.createfailure").show();
        $("div.createfailure").fadeIn(300).delay(1500).fadeOut(2000);
    }
    //else if (walletInfo.BankAccountNumber.length <= 17 && walletInfo.BankAccountNumber.length >= 4) {

    //    $("div.createfailure3").show();
    //    $("div.createfailure3").fadeIn(300).delay(1500).fadeOut(2000);
    //}
    else if (walletInfoes.BankRoutingNumber == "") {

        $("div.createfailure1").show();
        $("div.createfailure1").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.BankAccountNumber == "") {

        $("div.createfailure2").show();
        $("div.createfailure2").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else {
        $.ajax({
            url: url,
            type: "POST",
            dataType: "Json",
            async: true,
            data: { walletInfoes },
            success: function (response) {
                if (response != null) {
                  

                    var personName = response.customer.firstName + " " + response.customer.lastName;

                    var walletDetails = {
                        /* Start Bank Details */
                        method: "bank", // very important to set this as "bank" for ACH payments
                        bank_name: walletInfoes.BankName, // bank name, e.g. "Chase"
                        bank_account: walletInfoes.BankAccountNumber, // bank account number
                        bank_routing: walletInfoes.BankRoutingNumber, // bank routing number
                        bank_type: walletInfoes.BankType, // "checking" or "savings"
                        bank_holder_type: walletInfoes.BankHolderType, // "personal" or "business"
                        /* End Bank Details */

                        person_name: personName,
                        phone: response.customer.phone,
                        address_1: response.customer.billingAddress1, // customer address line 1
                        address_2: response.customer.billingAddress2, // customer address line 2
                        address_city: response.customer.billingCity, // customer address city
                        address_state: response.customer.billingState, // customer address state
                        address_zip: response.customer.billingZip, // customer address zip
                        address_country: "USA", // customer address country
                        customer_id: response.fattMerchantCustId, // OPTIONAL customer_id -

                        validate: false,
                    };

                    var payementId;
                    // making our instance of fattjs
                    //var fattJs = new FattJs("SRC-Inc-03464d66bde7", {
                    var fattJs = new FattJs(WebPaymentsToken, {
                    });

                    // call tokenize api
                    fattJs
                        .tokenize(walletDetails)
                        .then((tokenizedPaymentMethod) => {
                            // tokenizedPaymentMethod is the tokenized payment record
                            console.log('successful tokenization:', tokenizedPaymentMethod);
                            payementId = tokenizedPaymentMethod.id;
                            var count = response.walletList.length;
                            var walletInfoId = response.walletList[count - 1].walletInfoID;
                            var customerId = response.customer.customerID;
                            var walletDetails = new Object();
                            walletDetails.WalletInfoId = walletInfoId;
                            walletDetails.CustomerID = customerId;
                            walletDetails.PaymentMethodID = payementId;
                            var url = $('#hdnUpdateWalletPaymentUrl').val();

                            $.ajax({
                                url: url,
                                type: 'POST',
                                dataType: 'json',
                                async: true,
                                data: walletDetails,
                                success: function (data, textStatus, xhr) {
                                    console.log(data);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    console.log('Error in Operation');
                                }
                            });

                           
                            $('#WalletList').dataTable({
                                data: response.walletList,
                                "columns": [

                                    {
                                        "render": function (data, type, full, meta) {
                                            return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                        }
                                    },

                                    { "data": "bankName" },
                                    { "data": "bankAccountNumber" },
                                  
                                    {
                                        "render": function (data, type, full, meta) {
                                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                                        }
                                    }

                                ],
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [3] },
                                    { "bSearchable": false, "aTargets": [3] }
                                ],

                                language: {
                                    "search": '<i class="fa fa-search"></i>',
                                    searchPlaceholder: "Search records",
                                    paginate: {
                                        next: '<i class="fa fa-angle-right"></i>',
                                        previous: '<i class="fa fa-angle-left"></i>'
                                    }
                                },

                                "searching": true,
                                "lengthChange": false,
                                "paging": true,
                                "info": true,
                                "iDisplayLength": 3,
                                "bDestroy": true

                            });

                            $('#create_wallet').modal('hide');
                            $('#wallet_list').modal('show');


                        })
                        .catch(err => {
                           
                            alert(err["errors"]);

                            var count = response.walletList.length;
                            var walletId = response.walletList[count - 1].walletInfoID;

                            var urlD = $('#hdnDeleteWallet').val();

                            $.ajax({
                                type: "Post",
                                url: urlD,
                                data: { walletId: walletId },
                                success: function (data, textStatus, xhr) {
                                    console.log(data);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    console.log('Error in Operation');
                                }
                            });
                            // handle errors here
                            console.log('unsuccessful tokenization:', err);
                        });


                  

                    $("#WalletInfo_BankName").val('');
                    $("#WalletInfo_BankAccountNumber").val('');
                    $("#WalletInfo_BankType").val('');
                    $("#WalletInfo_BankHolderType").val('');
                    $("#WalletInfo_BankRoutingNumber").val('');
                }
                else {
                    $('#create_wallet').modal('hide');
                    $('#wallet_list').modal('show');
                    $("div.failures").show();
                    $("div.failures").fadeIn(300).delay(1500).fadeOut(4000);
                }
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

});


$('#btnSaveCredit').on('click', function () {

    var url = $("#hdnCreditWalletListUrl").val();
    var WebPaymentsToken = $("#hdnWebPaymentsToken").val();
    var walletInfoes = new Object();
    walletInfoes.CreditCardHolderName = $("#WalletInfo_CreditCardHolderFName").val();
    walletInfoes.CreditCardNumber = $("#WalletInfo_CreditCardNumber").val();
    walletInfoes.CvvNumber = $("#WalletInfo_CvvNumber").val();

    walletInfoes.Month = $("#monthid option:selected").val();
    walletInfoes.Year = $("#yearid option:selected").val();
    walletInfoes.ServiceAccountNumber = $("#hdnServiceAccountNumber").val();
    if (walletInfoes.CreditCardHolderName == "") {

        $("div.createfailure").show();
        $("div.createfailure").fadeIn(300).delay(1500).fadeOut(2000);
    }

    else if (walletInfoes.CreditCardNumber == "") {

        $("div.createfailure1").show();
        $("div.createfailure1").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.CvvNumber == "") {

        $("div.createfailure2").show();
        $("div.createfailure2").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.Month == "") {

        $("div.createfailure4").show();
        $("div.createfailure4").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.Year == "") {

        $("div.createfailure5").show();
        $("div.createfailure5").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else {
        $.ajax({
            url: url,
            type: "POST",
            dataType: "Json",
            async: true,
            data: { walletInfoes },
            success: function (response) {
                if (response != null) {
                    $('#WalletListCredit').dataTable({
                        data: response.walletList,
                        "columns": [

                            {
                                "render": function (data, type, full, meta) {
                                    return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                }
                            },

                            { "data": "creditCardHolderName" },
                            { "data": "creditCardNumber" },
                            { "data": "month" },
                            { "data": "year" },
                            {
                                "render": function (data, type, full, meta) {
                                    return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                                }
                            }

                        ],

                        language: {
                            "search": '<i class="fa fa-search"></i>',
                            searchPlaceholder: "Search records",
                            paginate: {
                                next: '<i class="fa fa-angle-right"></i>',
                                previous: '<i class="fa fa-angle-left"></i>'
                            }
                        },
                        "lengthChange": false,
                        "paging": true,
                        "info": true,
                        "iDisplayLength": 3,
                        "bDestroy": true
                    });
                   

                    var personName = response.customer.firstName + " " + response.customer.lastName;
                    //var fattJs = new FattJs('SRC-Inc-03464d66bde7', {
                    var fattJs = new FattJs(WebPaymentsToken, {
                        number: {
                            id: 'fattjs-number',
                            placeholder: '0000 0000 0000 0000',
                            style: 'width: 90%; height:90%; border-radius: 3px; border: 1px solid #ccc; padding: .5em .5em; font-size: 91%;'
                        },
                        cvv: {
                            id: 'fattjs-cvv',
                            placeholder: '000',
                            style: 'width: 30px; height:90%; border-radius: 3px; border: 1px solid #ccc; padding: .5em .5em; font-size: 91%;'
                        }
                    });


                    // tell fattJs to load in the card fields
                    fattJs.showCardForm().then(handler => {
                        console.log('form loaded');

                        // for testing!
                        handler.setTestPan('');
                        handler.setTestCvv('');
                        var form = document.querySelector('form');
                        form.querySelector('input[name=monthid]').value = '';
                        form.querySelector('input[name=yearid]').value = '';
                        form.querySelector('input[name=CreditCardHolderFName]').value = '';
                        form.querySelector('input[name=CreditCardHolderLName]').value = '';
                        form.querySelector('input[name=PhoneNo]').value = '';
                    })
                        .catch(err => {
                            console.log('error init form ' + err);
                            // reinit form
                        });


                    var extraDetails = {
                        total: 10, // $10.00
                        method: 'card',
                        url: "https://omni.fattmerchant.com/#/bill/",
                        firstname: response.customer.firstName, // customer first name
                        lastname: response.customer.lastName, // customer last name
                        company: "", // customer company
                        email: "", // customer email - receipt will be sent automatically
                        month: walletInfoes.Month, // credit card expiration month
                        year: walletInfoes.Year, // credit card expiration year
                        phone: "5555555555", // customer phone number
                        address_1: response.customer.billingAddress1, // customer address line 1
                        address_2: response.customer.billingAddress2, // customer address line 2
                        address_city: response.customer.billingCity, // customer address city
                        address_state: response.customer.billingState, // customer address state
                        address_zip: response.customer.billingZip, // customer address zip
                        address_country: "USA", // customer address country
                        customer_id: response.fattMerchantCustId, // OPTIONAL customer_id -
                        validate: false,

                    };
                   
                    // call pay api
                    fattJs
                        .tokenize(extraDetails)
                        .then((tokenizedPaymentMethod) => {
                            // tokenizedPaymentMethod is the tokenized payment record
                            console.log('successful tokenization:', tokenizedPaymentMethod);
                        })
                        .catch(err => {
                            // handle errors here
                            console.log('unsuccessful tokenization:', err);
                        });
                   
                    $('#create_wallet_Credit').modal('hide');
                    $('#wallet_list').modal('show');

                    $("#WalletInfo_CreditCardHolderName").val('');
                    $("#WalletInfo_CreditCardNumber").val('');
                    $("#WalletInfo_CvvNumber").val('');

                }
                else {
                    $('#create_wallet_Credit').modal('hide');
                    $('#wallet_list').modal('show');
                    $("div.failures").show();
                    $("div.failures").fadeIn(300).delay(1500).fadeOut(4000);
                }
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

});

$('#btnApprove').on('click', function (e) {
    var status = $('input:radio[name=bankradio]:checked').val()
    if (status == 'AchCard') {
        var url = $("#hdnProcessPaymentUrl").val();
    }
    else {
        var url = $("#hdnProcessCreditPaymentUrl").val();
    }

    var walletId = $("#hdnWalletId").val();
    var amount = $("#hdnAmountBilled").val();
    var customerServiceId = $("#hdnCustomerServiceId").val();
    var statementId = $("#hdnStatementId").val();
    $(".loader-wrapper").addClass("show1");
    if (walletId == "") {
        $(".loader-wrapper").removeClass("show1");
        $("div.selectfailures").show();
        $("div.selectfailures").fadeIn(300).delay(1500).fadeOut(3000);
    }
    else {
       
        $.ajax({
            url: url,
            type: 'POST',
            async: true,
            dataType: 'json',
            data: { walletInfoId: walletId, totalAmount: amount },
            success: function (data) {
                if (data.success) {
                    var paymentModel = new Object();
                    paymentModel.Amount = data.total;
                    paymentModel.PaymentTypeName = data.method;
                    paymentModel.WalletInfoID = walletId;
                    paymentModel.PaymentTransId = data.id;
                    paymentModel.PaymentDate = data.created_at;
                    var url = $("#hdnSavePaymentsDetailsUrl").val();
                    $.ajax({
                        url: url,
                        type: 'POST',
                        async: true,
                        dataType: 'json',
                        data: { paymentModel, customerServiceId: customerServiceId, statementId: statementId },
                        success: function (dataresponse) {
                            $(".loader-wrapper").removeClass("show1");
                            if (dataresponse > 0) {
                                e.preventDefault();
                                $('#approve_wallet').modal('hide');
                                $('#wallet_list').modal('hide');
                                $('#ignismyModal').modal('show');
                                //$("div.success").show();
                                //$("div.success").fadeIn(300).delay(1500).fadeOut(4000);
                                
                                //setTimeout(function () {
                                //    $(".loader-wrapper").removeClass("show1");
                                //    var url = $("#hdnStatementInvoiceUrl").val();
                                //    window.location.href = url;
                                //}, 3000);
                            }
                            else {
                                
                                $("div.failure").show();
                                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            $(".loader-wrapper").removeClass("show1");
                            $("div.failure").show();
                            $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                            console.log('Error in Operation');
                        }
                    });

                }
                else {
                    var paymentModel = new Object();
                    paymentModel.Amount = data.total;
                    paymentModel.PaymentTypeName = data.method;
                    paymentModel.WalletInfoID = walletId;
                    paymentModel.PaymentTransId = data.id;
                    paymentModel.PaymentDate = data.created_at;
                    paymentModel.message = data.message;
                    var url = $("#hdnSaveDeclinePaymentsDetailsUrl").val();
                    $.ajax({
                        url: url,
                        type: 'POST',
                        async: true,
                        dataType: 'json',
                        data: { paymentModel, customerServiceId: customerServiceId, statementId: statementId },
                        success: function (dataresponse) {
                            $(".loader-wrapper").removeClass("show1");
                            if (dataresponse > 0) {
                                e.preventDefault();
                                $('#approve_wallet').modal('hide');
                                $('#wallet_list').modal('hide');
                                setTimeout(function () {
                                    $(".loader-wrapper").removeClass("show1");
                                    var url = $("#hdnStatementInvoiceUrl").val();
                                    window.location.href = url;
                                }, 3000);

                            }
                            else {
                                $("div.failure").show();
                                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log('Error in Operation');
                        }
                    });
                    $('#wallet_list').modal('hide');
                    $('#approve_wallet').modal('hide');
                  
                    $("div.failure").show();
                    $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);

                }
            },
            error: function (xhr, textStatus, errorThrown) {
                $(".loader-wrapper").removeClass("show1");
                $("div.failure").show();
                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                console.log('Error in Operation');
            }
        });

    }
});

$("#ignismyModal").on("hidden.bs.modal", function () {
    var url = $("#hdnStatementInvoiceUrl").val();
    window.location.href = url;
});

$('#btnPaynow').on('click', function () {
    
    $("label[for='lblBankAccountName']").text('');
    $("label[for='lblBankAccountNumber']").text('');
    $("label[for='lblBankHolderType']").text('');
    $("label[for='lblBankType']").text('');
    var walletId = $("#hdnWalletId").val();
    var url = $("#hdnGetWalletDetailsUrl").val();
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: 'json',
        data: { walletInfoId: walletId },
        success: function (data) {
            if (walletId == "") {
                $("div.selectfailures").show();
                $("div.selectfailures").fadeIn(300).delay(1500).fadeOut(3000);
            }
            else {
                
                var amount = $("#hdnAmountBilled").val();
                $("label[for='lblBankAccountName']").text(data.bankName);
                $("label[for='lblBankAccountNumber']").text(data.bankAccountNumber);
                $("label[for='lblBankHolderType']").text(data.bankHolderType);
                $("label[for='lblBankType']").text(data.bankType);
                $("label[for='lblAmount']").text(amount);
                $('#approve_wallet').modal('show');
              
                
            }

        },
    });



});

$('#btnPaynowcredit').on('click', function () {

    $("label[for='lblBankAccountName']").text('');
    $("label[for='lblBankAccountNumber']").text('');
    $("label[for='lblBankHolderType']").text('');
    $("label[for='lblBankType']").text('');

    var walletId = $("#hdnWalletId").val();
    var url = $("#hdnCreditGetWalletDetailsUrl").val();
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: 'json',
        data: { walletInfoId: walletId },
        success: function (data) {
            if (walletId == "") {
                $("div.selectfailures").show();
                $("div.selectfailures").fadeIn(300).delay(1500).fadeOut(3000);
            }
            else {
                var amount = $("#hdnAmountBilled").val();
                $("label[for='lblBankAccountName']").text(data.creditCardNickName);
                //$("label[for='lblAccountRouting']").text(data.bankRoutingNumber);
                //$("label[for='lblAccount']").text(data.bankAccountNumber);
                $("label[for='lblAmount']").text(amount);
                $('#approve_wallet').modal('show');
            }

        },
    });



});


var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select-custom");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function (e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function getWalletId($this) {
    $("#hdnWalletId").val($this.value);

}

//Numeric input validation
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

//Only numeric  can be paste
function process(input) {
    let value = input.value;
    let numbers = value.replace(/[^0-9]/g, "");
    input.value = numbers;
}

$("#btnCancel").on('click', function () {
    $("#hdnWalletId").val("");
});

$("#btnApproveCancel").on('click', function () {
    $("#hdnWalletId").val("");
});

function deletepayrecord(hdwalletid) {
   
    $("#hdwalletid").val(hdwalletid);
    $("#dasmodel").modal("show");
}
var Delete = function () {
   

    var id = $("#hdwalletid").val();
    var deleteurl = $('#hdndeletewalletrecord').val();
    $.ajax({
        type: "Post",
        url: deleteurl,
        data: { walletInfoId: id },
        success: function (response) {
            $('#WalletList').dataTable({
                data: response.walletInfos,
                "columns": [
                    {
                        "render": function (data, type, full, meta) {
                            return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                        }
                    },

                    { "data": "bankName" },
                    { "data": "bankAccountNumber" },
                    
                    {
                        "render": function (data, type, full, meta) {
                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                        }
                    }
                ],
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [3] },
                    { "bSearchable": false, "aTargets": [3] }
                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },

                "searching": true,
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 3,
                "bDestroy": true

            });


            $('#WalletListCredit').dataTable({
                data: response.walletInfosCredit,

                "columns": [
                    {
                        "render": function (data, type, full, meta) {
                            if ($("#hwaletid").val() == full.walletInfoID) {

                                return '<input type="radio" checked="checked"  id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                            }
                            else {
                                return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                            }

                        }
                    },
                    { "data": "creditCardHolderName" },
                    { "data": "creditCardNumber" },
                    { "data": "month" },
                    { "data": "year" },
                    {
                        "render": function (data, type, full, meta) {
                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                        }
                    }

                ],
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },

                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 3,
                "bDestroy": true

            });
            $("#dasmodel").modal("hide");
            $("div.failuress").show();
            $("div.failuress").fadeIn(300).delay(1500).fadeOut(4000);
        }
    })
}
$("#inline_content input[name='bankradio']").click(function () {
    if ($('input:radio[name=bankradio]:checked').val() == "AchCard") {
        $('#achdiv').css('display', 'block')
        $('#WalletListdiv').css('display', 'block')
        $('#creditdiv').css('display', 'none')
        $('#WalletListCreditdiv').css('display', 'none')
        //jq('#header').css("visibility", "hidden");
        //$('#header').css("visibility", "visible");
    }
    else if ($('input:radio[name=bankradio]:checked').val() == "CreditCard") {
        $('#creditdiv').css('display', 'block')
        $('#achdiv').css('display', 'none')
        $('#WalletListdiv').css('display', 'none')
        $('#WalletListCreditdiv').css('display', 'block')
    }
});

//PDF DOWNLOAD for statment view
function downloadpdf(statmentid) {
     
   
    $(".pdfs").css("opacity", 0.5);
    $(".pdfs").css("background-color", "A9A9A9");
    $('#primg').show();
    var pdfurl = $('#hdnGetChangeOrderDocumentHtml').val();

    $.ajax({
        type: 'POST',
        url: pdfurl,
        datatype: "html",
        data: { statmentid: statmentid },
        success: function (response) {
           
            var pdfurl2 = $('#hdnSendChangeOrderDocument').val();
            if (response != "") {
                $.ajax({
                    type: 'POST',
                    url: pdfurl2,
                    dataType: "JSON",
                    data: { chnageOrderDocument: response },
                    success: function (response) {
                        if (response != "") {
                            var sampleBytes = base64ToArrayBuffer(response.base64);
                            saveByteArray([sampleBytes], response.fileName + '.pdf');
                            $(".pdfs").css("opacity", "");
                            $(".pdfs").css("background-color", "");
                            $('#primg').hide();
                        }
                    },
                    error: function () {
                    }
                });
            }
        },
        error: function () {
        }
    });
}
//});
function base64ToArrayBuffer(base64) {
   
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

var saveByteArray = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    return function (data, name) {
        var blob = new Blob(data, { type: "octet/stream" }),
            url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = name;
        a.click();
        window.URL.revokeObjectURL(url);
    };
}());

$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });
});