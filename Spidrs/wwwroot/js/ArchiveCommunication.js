﻿



$(document).ready(function () {
    
    var tblcustomer = $('#tblCustomers').DataTable({

        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: 'Next &raquo;',
                previous: '&laquo; Previous'
            }
        },
        "searching": true,
        "pagingType": "input",
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": []
    });
    $(".loader-wrapper").removeClass("show1");
    rowCollection = [];
    // var table=  $("#tblCustomers TBODY").closest('tr')
    var id = $("#tblCustomers tbody tr:first").find('.chkRow').val();
    // $("#tblCustomers tbody tr:first").find('.chkRow').prop('checked', true);
    $("#hdcustomerid").val(id);
    var UrlShowamessageDetails = $('#hdShowamessageDetails').val();
    //var rowIndex = this.parentElement.parentElement.rowIndex;
    //if (rowCollection.indexOf(rowIndex) > -1) {
    //    return false;
    //}
    $.ajax({
        type: "Post",
        url: UrlShowamessageDetails,
        data: { id: id },
        success: function (result) {

            if (result == 0) {
               
            }
            else {
                var row = '';
                $.each(result, function (key, value) {

                    if (value != null) {
                        row += `<div class="row mt-3">
                                    <div class="col-md-12 pt-left">
                                        
                                        Sent: <span class="bold" id="subjectdispaly">`+ value.senttodate + `</span><br>
                                        From: <span class="bold" id="subjectdispaly">`+ value.adminEmail + `</span><br>
                                        To: <span class="bold" id="subjectdispaly">`+ value.customerEmail + `</span><br>
                                        Subject: <span class="bold" id="subjectdispaly">`+ value.subject + `</span>
                                    </div>
                                    <div class="col-md-6 text-right f12 pt-left">
                                        <span class="text-primary bold" id="fulldate" style="display:none">`+ value.createdDate1 + `</span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <p class="text-justify" id="fullmsg" style="padding-left:15px;">`+ value.messages + `</p>
                                    </div>
                                </div>`
                    }

                });
                
                $('#subjectdata').append(row);
               
                $('#openre').css("visibility", "visible");
                $('#reload').css("visibility", "visible");
                $('#inboxhedaer').css("visibility", "visible");
               
            }
        }
    })


    $("#chkAll").click(function () {

        //Determine the reference CheckBox in Header row.
        var chkAll = this;

        //Fetch all row CheckBoxes in the Table.
        var chkRows = $("#tblCustomers").find(".chkRow");

        //Execute loop over the CheckBoxes and check and uncheck based on
        //checked status of Header row CheckBox.
        chkRows.each(function () {
            $(this)[0].checked = chkAll.checked;
        });
    });

    $(".chkRow").click(function () {

        //Determine the reference CheckBox in Header row.
        var chkAll = $("#chkAll");

        //By default set to Checked.
        chkAll.prop("checked", true);

        //Fetch all row CheckBoxes in the Table.
        var chkRows = $("#tblCustomers").find(".chkRow");

        //Execute loop over the CheckBoxes and if even one CheckBox 
        //is unchecked then Uncheck the Header CheckBox.
        chkRows.each(function () {
            if (!$(this).is(":checked")) {
                //chkAll.removeAttr("checked", "checked");
                chkAll.prop("checked", false);
                return;
            }
        });
    });
    SearchText();

});

function SearchText() {

   

    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary

    
    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}
function allmsg(id) {

    var rowIndex = this.event.currentTarget.parentElement.parentElement.rowIndex;
    var rowIndex1 = this.event.currentTarget.parentElement.parentElement.rowIndex;
    if (rowCollection.indexOf(rowIndex) > -1) {
        $("#hdcustomerid").val(id);

        return false;
    }
    rowCollection.push(rowIndex);
    $("#hdcustomerid").val(id);
    $('#replydiv').css('display', 'none');

    $('table#tblCustomers > tbody > tr').eq(--rowIndex1).removeClass("pink-bg");

    var UrlShowamessageDetails = $('#hdShowamessageDetails').val();
    $.ajax({
        type: "Post",
        url: UrlShowamessageDetails,
        data: { id: id },
        success: function (result) {

            $('#subjectdata').html('');
            //$('#subjectdata1').html('');
            if (result == 0) {
                $('#openre').css("visibility", "hidden");
                $('#reload').css("visibility", "hidden");
                //$('#inboxhedaer').css("visibility", "hidden");
            }
            else {

                var row = `<tr class="c" id="rowAppend_` + id + `"> <td colspan="7" class="inner-wrap"><div classs="container-fluid"> `;
                $.each(result, function (key, value) {

                    if (value.cusFirstName != null) {
                        name = value.cusFirstName + ' ' + value.cusLastName;
                    }
                    else {
                        name = value.adminFirstName + ' ' + value.adminLastName;
                    }
                    row += `  ${key == 0 ? ` <div class="row mt-3">
                                    <div class="col-md-6 pt-left">
                                         &nbsp;
                                   </div>
                                       
                                   
<div class="col-md-6"> &nbsp;
 
                                   </div> 
</div>

`: ``} 
<div class="row">
                                <div class="cst-subject col-md-6" style="padding-left:30px;line-height:20px;">
                                         Sent <span class="bold" id="subjectdispaly">` + value.senttodate + `</span><br>
From : <span> `+ name + `</span>  
                                         
                                    </div>
                                    <div class="col-md-6 text-right f12">
                                        <span class="text-primary bold" id="fulldate" style="display:none">`+ value.createdDate1 + `</span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-lg-12">
                                        <p class="text-justify bold" id="fullmsg" style="padding: inherit; white-space: break-spaces;">`+ value.messages + `</p>
                                    </div>
                                </div> 
`
                });
                row += `</div></td></tr>`;

                $('table#tblCustomers > tbody > tr').eq(--rowIndex).after(row);
                $('#subjectdata').append(row);

                $('#openre').css("visibility", "visible");
                $('#reload').css("visibility", "visible");
                $('#inboxhedaer').css("visibility", "visible");




            }
        }
    })

}


//delete message 
function Alldelete() {
    
    if ($('#tblCustomers').find('input[type=checkbox]:checked').length == 0) {
        //alert('Please select atleast one checkbox');
        $('#MyModalcheckbox').modal('show');
        return false;
    }
    $('#MyModalDelete').modal('show');
}

function Deletemsg() {

    var messageids = '';
    var table = $('#tblCustomers');
    table.DataTable().destroy();
    $("#tblCustomers TBODY TR").each(function (index, value) {
        var row = $(this);

        var checkstatus = row.closest('tr').find('.chkRow').prop('checked');
        if (checkstatus == true) {
            messageids += (!messageids ? row.find(".chkRow").val() : (',' + row.find(".chkRow").val()));
            
        }
       
    });
    $(".loader-wrapper").addClass("show1");
    var UrlDeletemsg = $('#hdDeletemsg').val();
    $(".loader-wrapper").addClass("show1");
    $.ajax({
        type: "Post",
        url: UrlDeletemsg,
        data: { messageids: messageids },
        success: function (result) {
            $('#MyModalDelete').modal('hide');
            $("div.deletemsgfailure").show();
            $("div.deletemsgfailure").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {
                
                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);

           
        },
        error: function (response) {

        }
    });
}





