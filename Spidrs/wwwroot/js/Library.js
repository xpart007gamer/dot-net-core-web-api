﻿$(document).ready(function () {
    $.fn.dataTable.moment('MM/DD/YYYY hh:mm:ss A');
    $.ajax({
        type: "Post",
        url: $("#hdnDocumentList").val(),
        dataType: "json",
        success: function (response) {
            $('#tblAgreementList').dataTable({
                data: response,
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "searching": true,
                "lengthChange": false,
                "paging": true,
                "info": true,
                "order": [],
                "columns": [
                    { "data": "roleName", "name": "roleName", "autoWidth": true }
                    , { "data": "documentName", "name": "documentName", "autoWidth": true }
                    , { "data": "tagName", "name": "tagName", "autoWidth": true },
                    {
                        "data": "createdDate",
                        "render": function (data) {

                            if (data == null) {
                                return '<label></label>';
                            }
                            else {
                                return moment(data).format('MM/DD/YYYY hh:mm:ss A');
                            }
                        }
                    }
                    , {
                        "data": "documentPath", "name": "documentPath", "autoWidth": true,
                        mRender: function (data, type, full) {
                            return '<a href="/DocumentFile/' + data + '" target="_blank" class="button3">View</a>';
                        }
                    }
                    , {
                        "data": "action", "name": "action", "autoWidth": true,
                        mRender: function (data, type, full) {
                            var str = '<a onclick="DeletePOP(' + full.id + ');return false;" style="cursor:pointer;"> <span class="fa fa-trash f16 text-danger"></span></a>'
                            if (full.roleName == 'BackOffice User') {
                                str = str + '<a onclick="DocumentforwardPOP(' + full.id + ');return false;"> <img style="height:20px; width:20px; cursor:pointer;" src="/img_new/icon-forward.png" /></a>';
                            }
                            return str;
                        }
                    }
                ]
            });

            $(".loader-wrapper").removeClass("show1");

            setTimeout(function () {
                $(".loader-wrapper").removeClass("show1");
            }, 10000);
        },
        error: function (response) {
            $(".loader-wrapper").removeClass("show1");
        }
    });

    // binddropdown();
    $(".loader-wrapper").removeClass("show1");
    $('#divcustomer').hide();
    $("#ddlskip").hide();
    $("#ddlskipF").hide();
});
///AutoComplete Functionality
$(document).ready(function () {
    SearchText();
});
function SearchText() {

    $("#txtCustomer").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomer").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }


    $("#txtBackOfficeCustomerID").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtBackOfficeCustomerID").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary


    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}
//Add dyanamic row
$(document).on('click', '#btnAddNewRow', function () {

    var firstRow = $(this).closest('.row').eq(0);
    $('#hdnHtml').html('');
    $('#hdnHtml').html($(firstRow).html());
    $('#hdnHtml').html($('#hdnHtml').html().replace('AgreementType', 'AgreementType' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('Tag', 'Tag' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('fileupload', 'fileupload' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('btnDeleteNewRow', 'btnDeleteNewRow'));
    $('#btnDeleteNewRow').css('display', 'block');
    _dvPreviewId = makeid(5);
    $('#hdnHtml').html($('#hdnHtml').html().replace('dvPreview', 'dvPreview' + _dvPreviewId + ''));
    $('#dvPreview' + _dvPreviewId + '').children().remove();
    $('#hdnHtml').html($('#hdnHtml').html().replace('errmsg1', 'errmsg1' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('errwebsite', 'errwebsite' + makeid(5) + ''));
    $(this).closest('.row').after('<div class="row table">' + $('#hdnHtml').html() + '</div>');
    //
    //$(this).parents('#Advertismenttable .row:last').find('.action-button').remove();
    $('#hdnHtml').html('');
});


//Generate rendom number
function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
///Save the Multiple data images and websiteurl
function Saveall() {
    var RoleName = "";
    var DocumentType = "";
    var AgreementType = "";
    var Tag = "";
    var file = "";
    //var BackofficeCustomerID = "";
    RoleName = $('#ddlRole').val();
    DocumentType = $('#ddlDocumentType').val();
    //BackofficeCustomerID = $('#txtCustomer').val();
    //if (BackofficeCustomerID == 'NULL') {

    //}
    //else if (BackofficeCustomerID == '') {
    //    $('#myModalAllCustomer').show();

    //    return false;
    //}

    var formData1 = new FormData();
    formData1.append("ID", 1);
    formData1.append("RoleName", RoleName);
    formData1.append("DocumentType", DocumentType);
    //  formData1.append("BackofficeCustomerID", BackofficeCustomerID);
    $("#Advertismenttable .row").each(function (index, value) {
        var row = $(this);
        file = row.find(".fileclass")[0].files[0];
        AgreementType = row.find(".AgreementType").val()
        Tag = row.find(".Tag").val()
        var errorimg = row.find(".errorimg").attr('id')
        var errorweb = row.find(".errorweb").attr('id')
        if (AgreementType == "") {
            $("#" + errorweb).html("Please enter document name").show()
            event.preventDefault()
        }
        else if (file == "" || file == undefined) {
            event.stopPropagation();
            $("#" + errorimg).html("Please select document").show()
            event.preventDefault()
        }

        else {

            formData1.append(row.find(".fileclass")[0].files[0].name, row.find(".fileclass")[0].files[0]);
            formData1.append("AgreementType", row.find(".AgreementType").val());
            formData1.append("Tag", row.find(".Tag").val());
            formData1.append("ordernums", row.find(".ordernumbers").val());
        }





    });
    if (AgreementType != "" && file != "" && file != undefined) {
        var url = $("#hdSaveMultipleDocument").val();
        $(".loader-wrapper").addClass("show1");
        $.ajax({
            type: "Post",
            url: url,
            contentType: false,
            processData: false,
            data: formData1,
            success: function (result) {
                if (result == 1) {
                    $(".loader-wrapper").removeClass("show1");
                    $("div.success").show();
                    $("div.success").fadeIn(400).delay(1600).fadeOut(3000);
                    setTimeout(function () {
                        var url = $("#hdnDocument").val();
                        window.location.href = url;
                    }, 300);
                }

                else {
                    $("div.failure").show();
                    $("div.failure").fadeIn(400).delay(1600).fadeOut(3000);
                }
            },
            error: function (response) {
            }
        });
    }
}

function ProceedSaveall() {

    var RoleName = "";
    var DocumentType = "";
    var AgreementType = "";
    var file = "";
    var file = "";
    var BackofficeCustomerID = "";
    RoleName = $('#ddlRole').val();
    DocumentType = $('#ddlDocumentType').val();
    BackofficeCustomerID = $('#txtCustomer').val();


    var formData1 = new FormData();
    formData1.append("ID", 1);
    formData1.append("RoleName", RoleName);
    formData1.append("DocumentType", DocumentType);
    formData1.append("BackofficeCustomerID", BackofficeCustomerID);
    $("#Advertismenttable TBODY TR").each(function (index, value) {
        var row = $(this);
        file = row.find(".fileclass")[0].files[0];
        AgreementType = row.find(".AgreementType").val()
        var errorimg = row.find(".errorimg").attr('id')
        var errorweb = row.find(".errorweb").attr('id')
        if (AgreementType == "") {
            $("#" + errorweb).html("Please enter document name").show()
            event.preventDefault()
        }
        else if (file == "" || file == undefined) {
            event.stopPropagation();
            $("#" + errorimg).html("Please select document").show()
            event.preventDefault()
        }

        else {

            formData1.append(row.find(".fileclass")[0].files[0].name, row.find(".fileclass")[0].files[0]);
            formData1.append("AgreementType", row.find(".AgreementType").val());
            formData1.append("Tag", row.find(".Tag").val());
            formData1.append("ordernums", row.find(".ordernumbers").val());
        }

    });
    if (AgreementType != "" && file != "" && file != undefined) {
        var url = $("#hdSaveMultipleDocument").val();
        $(".loader-wrapper").addClass("show1");
        $.ajax({
            type: "Post",
            url: url,
            contentType: false,
            processData: false,
            data: formData1,
            success: function (result) {
                if (result == 1) {
                    $(".loader-wrapper").removeClass("show1");
                    $("div.success").show();
                    $("div.success").fadeIn(400).delay(1600).fadeOut(3000);
                    setTimeout(function () {
                        var url = $("#hdnDocument").val();
                        window.location.href = url;
                    }, 300);
                }

                //else {

                //    $("div.failure").show();
                //    $("div.failure").fadeIn(400).delay(1600).fadeOut(3000);
                //}
            },
            error: function (response) {

            }
        });
    }
}

function AllCustomerWarningPOP() {

    $("#myModalAllCustomer").hide();
    return false;
}
function DeletePOP(ID) {

    $('#agreementid').val(ID);
    $('#myModal45656').modal('show');
}
function Delete() {
    var id = $('#agreementid').val();
    alert(id);
}
function Delete() {

    var id = $('#agreementid').val();

    var url = $('#hdnDeleteDocument').val();
    $(".loader-wrapper").addClass("show1");
    $.ajax({
        type: "Post",
        url: url,
        data: { id: id },
        success: function (result) {
            if (result == true) {
                $(".loader-wrapper").removeClass("show1");
                $("#myModal45656").modal("hide");
                $("div.deletemsgfailure").show();
                $("div.deletemsgfailure").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdnDocument").val();
                    window.location.href = url;
                }, 3000);
            }
            else {
                $("#MyModalMail").modal("hide")
                $("div.failure").show();
                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdnDocument").val();
                    window.location.href = url;
                }, 3000);
            }
        },
        error: function (response) {
            $(".loader-wrapper").removeClass("show1");
        }
    });
}
$(document).on('click', '#btnDeleteNewRow', function () {
    var rowId = $(this).closest('.row');
    rowId.remove();
});


///validation check
$('#checkvalidation').click(function () {

    var WebsitePath = $('#WebsitePath').val();
    if (WebsitePath == "") {
        $("#errmsg").html("Please Select Website Path").show().fadeOut(3000);
        return false;
    }
    var ID = $('#ID').val();
    if (ID == 0 || ID == "" || ID == undefined) {
        var file1 = $("#fileupload")[0].files[0]

        if (file1 == "" || file1 == undefined) {
            $("#errmsg1").html("Please Select Image").show().fadeOut(3000);
            return false;
        }
    }
});

////Check the image duplicate
function Checkduplicateimage() {

    var file1 = $("#" + fileupload + "")[0].files[0]
    if (file1 == "" || file1 == undefined) {
        $("#" + errormsg + "").html("Please Select Image").show().fadeOut(3000);
        return false;
    }
    var formData = new FormData();
    formData.append("checkimage", file1);
    var url = $('#hdnCheckexistimage').val();
    $.ajax({
        type: "Post",
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            if (result > 0) {
                $("#" + errormsg + "").html("This image already exist.").show();
                $("#" + fileupload + "").val('');
                $("#" + fileupload + "").text('');
                $("#" + imageshowdiv + "").html("");
                return false;
            }
        }
    });
}

function show(input) {

    var DocumentType = $('#ddlDocumentType').val();
    if (DocumentType == 3) {
        var validExtensions = ['JPG', 'PNG', 'JPEG', 'GIF', 'PDF', 'TIF', 'TIFF', 'BMP', 'EPS']; //array of valid extensions
        var fileName = input.files[0].name;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1).toUpperCase();
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            input.type = ''
            input.type = 'file'

            alert("Only these file types are accepted : " + validExtensions.join(', '));
        }
    }
    if (DocumentType == 1 || DocumentType == 2) {
        var validExtensions = ['PDF']; //array of valid extensions
        var fileName = input.files[0].name;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1).toUpperCase();
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            input.type = ''
            input.type = 'file'

            alert("Only these file types are accepted : " + validExtensions.join(', '));
        }
    }

}



//Advance Search
function AdvanceSearchPOP() {

    $("#MyModalAdvanceSearch").modal('show');
    return false;
}
function radioName(Name) {
    $("#lblAdvanceSearch").text(Name);
    var radioValue = $("input[name='radioName']:checked").val();
    if (Name == 'Skip') {
        $("#txtAdvanceSearch").hide();
        $("#txtAdvanceSearch").val('');
        $("#ddlskip").show();
    }
    else {
        $("#ddlskip").hide();
        $("#txtAdvanceSearch").show();
    }

}
function AdvanceSearchPOPClose() {
    $("#MyModalAdvanceSearch").modal('hide');
}
function AdvanceSearchValue() {
    var Value = "";


    var ColumnName = $("input[name='radioName']:checked").val();
    if (ColumnName == undefined) {
        $('#txtAdvanceSearch').css('border-color', 'red');
        $('#txtAdvanceSearch').focus();
        return false;
    }
    if (ColumnName == 'SkipService') {
        Value = $('#ddlskip').val();
    }
    else {
        Value = $('#txtAdvanceSearch').val();
    }

    $('#txtAdvanceSearch').css('border-color', 'none');


    var hdAdvanceSearch = $('#hdAdvanceSearch').val();
    $.ajax({
        type: "Post",
        url: hdAdvanceSearch,
        data: { ColumnName: ColumnName, Value: Value },
        dataType: "json",
        success: function (response) {

            $('#tblAdvanceSearch').dataTable({
                data: response,
                "columns": [


                    {
                        "render": function (data, type, full, meta) {

                            return '<input class="chkRowadvance" value="' + full.backOfficeCustomerID + '"  onclick="return AddForcheckAdvanceSearch(this,\'' + full.backOfficeCustomerID.toString() + '\')" class="chkBox" type="checkbox" />';
                        }
                    },
                    { "data": "backOfficeCustomerID" },
                    { "data": "name" },
                    { "data": "emailId" },
                    { "data": "serviceAccountNumber" },
                    { "data": "address" }



                ],
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true


            });
            // $('#tblAdvanceSearch').DataTable().destroy();

            setTimeout(function () {

            }, 1000);
        },
        error: function (response) {

        }
    });
}
function AddForcheckAdvanceSearch(checkbox, BackOfficeCustomerID) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#txtCustomer").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + BackOfficeCustomerID;
        } else
            hdcheckedList = BackOfficeCustomerID;

        $("#txtCustomer").val(hdcheckedList);
    }
    else {
        var single = $("#txtCustomer").val()
        var delList = $("#txtCustomer").val().split(',');
        var index = delList.indexOf(BackOfficeCustomerID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#txtCustomer").val(bbb);
    }
}

$("#chkAll").click(function () {

    //Determine the reference CheckBox in Header row.
    var chkAll = this;

    //Fetch all row CheckBoxes in the Table.
    var chkRows = $("#tblAdvanceSearch").find(".chkRowadvance");

    //Execute loop over the CheckBoxes and check and uncheck based on
    //checked status of Header row CheckBox.
    chkRows.each(function () {
        $(this)[0].checked = chkAll.checked;

    });
    if (chkAll.checked == true) {
        var values = "";

        $("#tblAdvanceSearch TBODY TR").each(function (index, value) {
            var row = $(this);

            var checkstatus = row.closest('tr').find('.chkRowadvance').prop('checked');

            values += (!values ? row.find(".chkRowadvance").val() : (',' + row.find(".chkRowadvance").val()));
            $("#txtCustomer").val(values);
        });

    }
    else {
        $("#txtCustomer").val(values);
    }
});

//Advance Search POP Forward
function AdvanceSearchPOPForward() {

    $("#MyModalAdvanceSearchForward").show();
    return false;
}
function radioNameF(Name) {
    $("#lblAdvanceSearchForward").text(Name);
    var radioValue = $("input[name='radioNameF']:checked").val();
    if (Name == 'Skip') {
        $("#txtAdvanceSearchForward").hide();
        $("#txtAdvanceSearchForward").val('');
        $("#ddlskipF").show();
    }
    else {
        $("#ddlskipF").hide();
        $("#txtAdvanceSearchForward").show();
    }

}
function AdvanceSearchForwardValue() {
    var Value = "";


    var ColumnName = $("input[name='radioNameF']:checked").val();
    if (ColumnName == undefined) {
        $('#txtAdvanceSearchForward').css('border-color', 'red');
        $('#txtAdvanceSearchForward').focus();
        return false;
    }
    if (ColumnName == 'SkipService') {
        Value = $('#ddlskipF').val();
    }
    else {
        Value = $('#txtAdvanceSearchForward').val();
    }

    $('#txtAdvanceSearchForward').css('border-color', 'none');


    var hdAdvanceSearch = $('#hdAdvanceSearch').val();
    $.ajax({
        type: "Post",
        url: hdAdvanceSearch,
        data: { ColumnName: ColumnName, Value: Value },
        dataType: "json",
        success: function (response) {

            $('#tblAdvanceSearchForward').dataTable({
                data: response,
                "columns": [


                    {
                        "render": function (data, type, full, meta) {

                            return '<input class="chkRowadvance" value="' + full.backOfficeCustomerID + '"  onclick="return AddForcheckAdvanceSearchForward(this,\'' + full.backOfficeCustomerID.toString() + '\')" class="chkBox" type="checkbox" />';
                        }
                    },
                    { "data": "backOfficeCustomerID" },
                    { "data": "name" },
                    { "data": "emailId" },
                    { "data": "serviceAccountNumber" },
                    { "data": "address" }



                ],
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true


            });
            // $('#tblAdvanceSearchForward').DataTable().destroy();

            setTimeout(function () {

            }, 1000);
        },
        error: function (response) {

        }
    });
}
function AddForcheckAdvanceSearchForward(checkbox, BackOfficeCustomerID) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#txtBackOfficeCustomerID").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + BackOfficeCustomerID;
        } else
            hdcheckedList = BackOfficeCustomerID;

        $("#txtBackOfficeCustomerID").val(hdcheckedList);
    }
    else {
        var single = $("#txtBackOfficeCustomerID").val()
        var delList = $("#txtBackOfficeCustomerID").val().split(',');
        var index = delList.indexOf(BackOfficeCustomerID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#txtBackOfficeCustomerID").val(bbb);
    }
}
function AdvanceSearchPOPCloseForward() {
    $("#MyModalAdvanceSearchForward").hide();
}
$("#chkAllF").click(function () {

    //Determine the reference CheckBox in Header row.
    var chkAll = this;

    //Fetch all row CheckBoxes in the Table.
    var chkRows = $("#tblAdvanceSearchForward").find(".chkRowadvance");

    //Execute loop over the CheckBoxes and check and uncheck based on
    //checked status of Header row CheckBox.
    chkRows.each(function () {
        $(this)[0].checked = chkAll.checked;

    });
    if (chkAll.checked == true) {
        var values = "";

        $("#tblAdvanceSearchForward TBODY TR").each(function (index, value) {
            var row = $(this);

            var checkstatus = row.closest('tr').find('.chkRowadvance').prop('checked');

            values += (!values ? row.find(".chkRowadvance").val() : (',' + row.find(".chkRowadvance").val()));
            $("#txtBackOfficeCustomerID").val(values);
        });

    }
    else {
        $("#txtBackOfficeCustomerID").val(values);
    }
});
////Check the image duplicate /////Update case Advertismnet
function CheckduplicateimageU() {
    var file1 = $("#fileupload1222")[0].files[0]
    if (file1 == "" || file1 == undefined) {
        $("#errmsg121").html("Please Select Image").show().fadeOut(3000);
        return false;
    }
    var formData = new FormData();
    formData.append("checkimage", file1);
    var url = $('#hdnCheckexistimage').val();
    $.ajax({
        type: "Post",
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            if (result > 0) {
                $("#errmsg121").html("This image already exist.").show();
                $("#fileupload1222").val('');
                $("#fileupload1222").text('');
                $("#dvPreview12").html("");
                return false;
            }
        }
    });
}

$("#ddlRole").change(function () {
    var ddlRole = $('#ddlRole').val();
    if (ddlRole == 'Customer') {
        $('#divcustomer').show();
        $('#txtCustomer').val('');
    }
    else {
        $('#divcustomer').hide();
        $('#txtCustomer').val('NULL');

    }
});


function DocumentforwardPOP(ID) {

    $('#hfforwardid').val(ID);

    $('#MyModalForward').modal('show');
}
function Documentforward() {

    var hfforwardid = $('#hfforwardid').val();
    var BackofficeCustomerID = $('#txtBackOfficeCustomerID').val();

    var url = $('#hdnDocumentforward').val();
    $(".loader-wrapper").addClass("show1");
    $.ajax({
        type: "Post",
        url: url,
        data: { ID: hfforwardid, BackofficeCustomerID: BackofficeCustomerID },
        success: function (result) {
            if (result == true) {
                $(".loader-wrapper").removeClass("show1");
                $("#MyModalForward").modal("hide")
                $("div.forward").show();
                $("div.forward").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdnDocument").val();
                    window.location.href = url;
                }, 3000);
            }
            else {
                $("#MyModalForward").modal("hide")
                $("div.failure").show();
                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdnDocument").val();
                    window.location.href = url;
                }, 3000);
            }
        },
        error: function (response) {
            $(".loader-wrapper").removeClass("show1");
        }
    });
}

function IsAlreadyDocumentName(DocumentName) {

    this;
    var Document = DocumentName.value;


    var url = $('#hdnIsAlreadyDocumentName').val();
    $(".loader-wrapper").addClass("show1");
    $.ajax({
        type: "Post",
        url: url,
        data: { Documents: Document },
        success: function (data) {
            if (data[0] != null && data[0] != "") {
                $("#" + DocumentName.id + "").val("");
                $("div.autoPayfailure").show();
                $("div.autoPayfailure").fadeIn(300).delay(1500).fadeOut(3000);

                $(".loader-wrapper").removeClass("show1");



            }
            else {
                $(".loader-wrapper").removeClass("show1");

            }
        },
        error: function (response) {
            $(".loader-wrapper").removeClass("show1");
        }
    });




}


