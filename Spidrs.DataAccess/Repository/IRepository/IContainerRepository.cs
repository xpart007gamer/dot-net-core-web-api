﻿using Spidrs.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository.IRepository
{
    public interface IContainerRepository
    {
        public List<ContainerList> ContainerList(int start, string searchvalue, int Length, string SortColumn, string sortDirection);
    }
}
