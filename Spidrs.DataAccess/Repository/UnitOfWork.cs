﻿using Spidrs.DataAccess.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _db;

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            User = new UserRepository(_db);
            Routes = new RoutesRepository(_db);
            Container = new ContainerRepository(_db);


            ApplicationUser = new ApplicationRepository(_db);
        }

        public IRoutesRepository Routes { get; private set; }
        public IContainerRepository Container { get; private set; }
        public IUserRepository User { get; private set; }

        public IApplicationRepository ApplicationUser { get; private set; }

        public void Save()
        {
            _db.SaveChanges();
        }

    }
}
