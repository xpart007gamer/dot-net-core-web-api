﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Spidrs.DataAccess.Repository.IRepository;
using Spidrs.Models;
using Spidrs.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository
{
    public class RoutesRepository : Repository<RouteList>, IRoutesRepository
    {
        private ApplicationDbContext _db;

        public RoutesRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public List<RouteList> RouteList(int start, string searchvalue, int Length, string SortColumn, string sortDirection)
        {

            SqlParameter usernameParam1 = new SqlParameter("@Pageno", start);
            SqlParameter usernameParam2 = new SqlParameter("@filter", searchvalue);
            SqlParameter usernameParam3 = new SqlParameter("@pagesize", Length);
            SqlParameter usernameParam4 = new SqlParameter("@Sorting ", SortColumn);
            SqlParameter usernameParam5 = new SqlParameter("@SortOrder", sortDirection);
            var routes = _db.Routes
            .FromSqlRaw("exec GetRoutes @Pageno, @filter,@pagesize,@Sorting,@SortOrder", usernameParam1, usernameParam2, usernameParam3, usernameParam4, usernameParam5)
            .ToList();
            return routes;
        }
    }
}
