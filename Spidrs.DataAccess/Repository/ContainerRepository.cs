﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Spidrs.DataAccess.Repository.IRepository;
using Spidrs.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository
{
    public class ContainerRepository : Repository<ContainerList>, IContainerRepository
    {
        private ApplicationDbContext _db;

        public ContainerRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public List<ContainerList> ContainerList(int start, string searchvalue, int Length, string SortColumn, string sortDirection)
        {

            SqlParameter pageNo = new SqlParameter("@Pageno", start);
            SqlParameter filter = new SqlParameter("@filter", searchvalue);
            SqlParameter pageSize = new SqlParameter("@pagesize", Length);
            SqlParameter sorting = new SqlParameter("@Sorting ", SortColumn);
            SqlParameter sortOrder = new SqlParameter("@SortOrder", sortDirection);
            var routes = _db.Containers
            .FromSqlRaw("exec GetContainers @Pageno, @filter,@pagesize,@Sorting,@SortOrder", pageNo, filter, pageSize, sorting, sortOrder)
            .ToList();
            return routes;
        }
    }
}
