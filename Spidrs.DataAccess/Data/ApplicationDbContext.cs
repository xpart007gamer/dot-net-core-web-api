﻿
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Spidrs.Models;
using Spidrs.Models.ViewModels;

namespace Spidrs.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) 
        {
             
        }

        public DbSet<Driver> Drivers { get; set; }
        public DbSet<RouteList> Routes { get; set; }
        public DbSet<ContainerList> Containers { get; set; }
        public DbSet<RouteListApi> RoutesApi { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RouteList>().HasNoKey();
            modelBuilder.Entity<ContainerList>().HasNoKey();
            modelBuilder.Entity<RouteListApi>().HasNoKey();
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<IdentityUserLogin<string>>().HasNoKey();
            //modelBuilder.Entity<IdentityUserRole<string>>().HasNoKey();
            //modelBuilder.Entity<IdentityUserToken<string>>().HasNoKey();
        }
    }
}
