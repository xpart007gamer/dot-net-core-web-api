﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Spidrs.DataAccess;
using Spidrs.DataAccess.Repository.IRepository;
using Spidrs.Models.ViewModels;

namespace Spiders.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetRoutesController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ApplicationDbContext _db;

        public GetRoutesController(ILogger<WeatherForecastController> logger, IConfiguration configuration, ApplicationDbContext db)
        {
            _logger = logger;
            _configuration = configuration;
            _db=db;
        }


        [HttpPost(Name = "GetRoutesByWeek")]
        public async Task<IEnumerable<RouteListApi>> Post(string days)
        {
            var dayNames = days.Split(',');
            string formattedDays = "";
            foreach (var day in dayNames)
            {
                if(day != dayNames.Last())
                {
                    formattedDays += "'" + day + "'"+ ",";
                }
                else
                {
                    formattedDays += "'" + day + "'";
                }
               
            }
            //var query = @"select a.routeID,a.route,a.dayofweek,b.frequency from AllRoutes A inner join AllContainers B on A.dayofweek=B.dayofweek
            //where A.dayofweek in(" + String.Join(",", days) + @") order by A.daynumeric";
            var query = @"select a.routeID,a.route,a.dayofweek,b.frequency from AllRoutes A inner join AllContainers B on A.dayofweek=B.dayofweek
            where A.dayofweek in(" + String.Join(",", formattedDays) + @") order by A.daynumeric";
           
            var data = await _db.RoutesApi.FromSqlRaw(query).ToListAsync();
            return data;
        }
    }
}
